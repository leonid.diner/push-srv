import requests
from pusher_push_notifications import PushNotifications
import config
#send_push(token, scenario, platform)



def send_push(body, pushProvider, platform, iosDeviceToken):

     server_key = 'key=AAAAL9t3s3I:APA91bGWHZudwd2GMbidmKdl-wUQEtlv44rC3MWrkuRC3C-hFfmqc5Uuh2s5FPCrd9Un49I0mJN-F7z76zYCqTwgIF0krSLOnNC-XjjKRyF1O9LYaoDLmEdrF9zJAT82o1nlTyendpau'
     if platform.lower() == "android":
         url = "https://fcm.googleapis.com/fcm/send"
         headers = {
              'Authorization': server_key,
              'Content-Type': 'application/json'
          }
     else:
         if pushProvider.lower() == "APN":
              url = "https://api.development.push.apple.com/3/device/" + iosDeviceToken
              headers = {
                  'apns-topic': 'com.appsflyer.universallinks',
                  'apns-expiration': '1',
                  'apns-priority': '10',
              }
         else:
             url = "https://fcm.googleapis.com/fcm/send"
             headers = {
                 'Authorization': server_key,
                 'Content-Type': 'application/json'
             }

     print("in sending push")
     # payloadScenario = body

     response = requests.request("POST", url, headers=headers, data=body)
     print(response.text)
     print("in push been sent")

#
# APN EXAMPLE
#
# curl -v -d '{"aps":{"alert":"Testing.. (5)","badge":1,"sound":"default", "to":
#     "deNQJZDB8E1riyze4XFDDm:APA91bG_pyttNLVY_8MqTmTrdASFywH4iHy2H145Tuqw4xISvdDEMpbO_mPJw_XbDBf_3jXgoIeIEtteFc0tpyC2HjdZLy5xiOI0lTn0R3_fjL1eA4fw0cUKSjO818gRNlc3LypbRLwf",
#    "notification": {
#       "title": "Hello",
#       "body": "Yellow"
#    },
#    "data": {"key2":{"key1":{
#            "af_dep": "https://automationsdk.blaster.afsdktests.com/M5Ax/1bdc75b4",
#            "af_dep2": "https://click.sflink.afsdktests.com/?qs=ca700f1872c5072f6e953f55037f3a2f5933f2c1e51bd84bf9a1f060ac6e25b4c74546e513392a24eb948c9997bb1795"
#            }}}
# }}' -H "apns-topic:<BUNDLE_ID>" -H "apns-expiration: 1" -H "apns-priority: 10" --http2 --cert-type P12 --cert <CERTIFICATE:password> https://api.development.push.apple.com/3/device/<DEVICE_TOKEN>
#
