import configparser
import os
import json
import requests

config = configparser.ConfigParser()
config.read("config.ini")

for section in config.sections():
    print("Section: %s" % section)
    for options in config.options(section):
        print("x %s:::%s:::%s" % (options,
                                  config.get(section, options),
                                  str(type(options))))

def get_slack_channel_name():
    return config.get("Slack", "slack_channel_name")

def get_delta_time():
    return config.get("ttl", "delta_time")

def get_slack_service_url():
    return config.get("Slack", "slack_service_url")

def get_credentials_to_postgress():
    if "TOKEN" in os.environ:
        TOKEN = os.environ["TOKEN"]
        res = requests.get(url="http://vault.eu1.appsflyer.com:8200/v1/secret/appsflyer-stg-postgress",
                           headers={"X-Vault-Token": TOKEN})
        postgress_api_token = json.loads(res.content.decode("utf-8"))["data"]
        return postgress_api_token
    else:
        print("no TOKEN available!!!")
        return None

def get_url_with_credentials_to_db():
    response = get_credentials_to_postgress()
    engine_user = response['user']
    engine_passwrod = response['password']

    res_url = requests.post('http://url_proxy.eu1.appsflyer.com:6996/get_db_url', json={'user': engine_user,'password': engine_passwrod})
    url_to_db = res_url.text
    if 'appsflyer.com' in url_to_db:
        url_with_credentials = 'postgresql+psycopg2://{engine_user}:{engine_passwrod}@{url_to_db}'.format(url_to_db=url_to_db, engine_user=engine_user, engine_passwrod=engine_passwrod)
        return url_with_credentials
    else:
        error_message = "DB URL wasn't received"
        print(error_message)
        return None