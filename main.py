#!/usr/bin/python3.6

import logging
import os
import sys
import threading
from logging.handlers import RotatingFileHandler
# import requests
from flask import request, Response, send_file, make_response, jsonify
# from urllib3 import response
import automation_logic
from app import app
import json
# from push_json_scenarios import scenariosAndroid, scenariosIOS

LOG_FILE = "mobile_automation_service.log"

@app.errorhandler(500)
def internal_server_error(error):
    app.logger.error('Server Error: %s', (error))
    return Response(str(error), status=500)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.error('Page not found: %s', (request.path))
    return Response(str(error), status=404)

@app.route('/health-check', methods=['GET', 'HEAD'])
def health_check():
    app.logger.info("health-check")
    return Response("ok", status=200)

# ################# hand shake for getting push notification TOKEN  scenario ########################
# @app.route('/what-push-notification-scenario-id', methods=['GET'])
# def get_push_scenario():
#     app.logger.info("PUSH_NOTIFICATION_TOKEN-scenario-id")
#     token = automation_logic.get_token_by_test_id(request)
#     return (token, 200)

@app.route('/push-notification-sender-scenario', methods=['POST'])
def set_push_notification_ios():
    def do_work(value, body, pushProvider, platform, deviceToken):
        import time
        time.sleep(value)
        automation_logic.send_push(body,  pushProvider, platform, deviceToken)
    if 'pushDelayTime' in request.args:
        value = float(request.args['pushDelayTime'])
        thread = threading.Thread(target=do_work, kwargs={'value': value, 'body' : request.data.decode(), 'pushProvider':request.args['pushProvider'], 'platform' : request.args['platform'], 'deviceToken' :request.args['deviceToken']})
        thread.start()
        return 'push will be sent'
    else:
        automation_logic.send_push(request.data.decode(),  request.args['pushProvider'], request.args['platform'], request.args['deviceToken'])
        return 'push sent'
#
# @app.route('/add-new-push-scenario', methods=['POST'])
# def new_push_scenario():
#     if request.json["platform"].lower() == "Android":
#         scenariosAndroid[request.json["scenarioName"]] = request.json["scenarioBody"]
#     else:
#         scenariosIOS[request.json["scenarioName"]] = request.json["scenarioBody"]
#     app.logger.info("add new scenario")
#     return Response("ok", status=200)

if __name__ == '__main__':
    handler = RotatingFileHandler(LOG_FILE, maxBytes=10000000, backupCount=5)
    sys.path.append(os.path.dirname(os.path.realpath(__file__)))
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    # app.run(port=5000, host='127.0.0.1')
    # app.run(port=8080, host=socket.gethostbyname('localhost'))
    app.run(port=8185, host='10.50.1.78')
    # app.run(port=8080, host='mob-srv-defualt.eu1.appsflyer.com')
    # mob - srv - defualt.eu1.appsflyer.com: 8080





